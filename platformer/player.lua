player_start_x = 360
player_start_y = 100

player = world:newRectangleCollider(
  player_start_x,
  player_start_y,
  40,
  100,
  { collision_class = "Player" }
)
player:setFixedRotation(true)
player.animation = animations.idle
player.is_moving = false
player.direction = 1
player.grounded = true
player.speed = 240

function player_update(dt)
  if player.body then
    local colliders = world:queryRectangleArea(
      player:getX() - 20,
      player:getY() + 50,
      40,
      2,
      { "Platform" }
    )
    player.grounded = #colliders > 0

    player.is_moving = false

    local px, _ = player:getPosition()
    if love.keyboard.isDown("right", "d") then
      player:setX(px + player.speed * dt)
      player.is_moving = true
      player.direction = 1
    end
    if love.keyboard.isDown("left", "a") then
      player:setX(px - player.speed * dt)
      player.is_moving = true
      player.direction = -1
    end

    if player:enter "Danger" then player:setPosition(player_start_x, player_start_y) end
  end

  if player.grounded then
    if player.is_moving then
      player.animation = animations.run
    else
      player.animation = animations.idle
    end
  else
    player.animation = animations.jump
  end

  player.animation:update(dt)
end

function player_draw()
  local px, py = player:getPosition()
  player.animation:draw(
    sprites.playerSheet,
    px,
    py,
    nil,
    0.25 * player.direction,
    0.25,
    130,
    300
  )
end
