function love.load()
  love.window.setMode(1000, 768)

  anim8 = require "libraries/anim8/anim8"
  sti = require "libraries/Simple-Tiled-Implementation/sti"
  camera_file = require "libraries/hump/camera"

  cam = camera_file()

  sounds = {
    jump = love.audio.newSource("audio/jump.wav", "static"),
    music = love.audio.newSource("audio/music.mp3", "stream"),
  }

  sounds.music:setLooping(true)
  sounds.music:setVolume(0.5)
  sounds.music:play()

  sprites = {
    playerSheet = love.graphics.newImage "sprites/playerSheet.png",
    enemySheet = love.graphics.newImage "sprites/enemySheet.png",
    background = love.graphics.newImage "sprites/background.png",
  }

  local grid = anim8.newGrid(
    614,
    564,
    sprites.playerSheet:getWidth(),
    sprites.playerSheet:getHeight()
  )
  local enemy_grid =
    anim8.newGrid(100, 79, sprites.enemySheet:getWidth(), sprites.enemySheet:getHeight())

  animations = {
    idle = anim8.newAnimation(grid("1-15", 1), 0.05),
    jump = anim8.newAnimation(grid("1-7", 2), 0.05),
    run = anim8.newAnimation(grid("1-15", 3), 0.05),
    enemy = anim8.newAnimation(enemy_grid("1-2", 1), 0.03),
  }

  wf = require "libraries/windfield/windfield"
  world = wf.newWorld(0, 800, false)
  world:setQueryDebugDrawing(true)

  world:addCollisionClass "Player"
  world:addCollisionClass "Platform"
  world:addCollisionClass "Danger"

  require "player"
  require "enemy"
  require "libraries/show"

  danger_zone =
    world:newRectangleCollider(-500, 800, 5000, 50, { collision_class = "Danger" })
  danger_zone:setType "static"

  platforms = {}

  flag_x = 0
  flag_y = 0

  save_data = { current_level = "level1" }
  if love.filesystem.getInfo "data.lua" then
    local data = love.filesystem.load "data.lua"
    data()
  end

  load_map(save_data.current_level)
end

function love.update(dt)
  world:update(dt)
  game_map:update(dt)

  player_update(dt)
  enemies_update(dt)

  local px, _ = player:getPosition()
  cam:lookAt(px, love.graphics.getHeight() / 2)

  local colliders = world:queryCircleArea(flag_x, flag_y, 10, { "Player" })
  if #colliders > 0 then
    if save_data.current_level == "level1" then
      load_map "level2"
    elseif save_data.current_level == "level2" then
      load_map "level1"
    end
  end
end

function love.draw()
  love.graphics.draw(sprites.background, 0, 0)

  cam:attach()

  game_map:drawLayer(game_map.layers["Tile Layer 1"])
  -- world:draw()

  player_draw()
  enemies_draw()

  cam:detach()
end

function love.keypressed(key)
  if (key == "up" or key == "w") and player.grounded then
    player:applyLinearImpulse(0, -4000)
    sounds.jump:play()
  end
end

function spawn_platform(x, y, width, height)
  if width > 0 and height > 0 then
    local platform =
      world:newRectangleCollider(x, y, width, height, { collision_class = "Platform" })
    platform:setType "static"
    table.insert(platforms, platform)
  end
end

function destroy_all()
  for i = #platforms, 1, -1 do
    if platforms[i] then
      platforms[i]:destroy()
      table.remove(platforms, i)
    end
  end

  for i = #enemies, 1, -1 do
    if enemies[i] then
      enemies[i]:destroy()
      table.remove(enemies, i)
    end
  end
end

function load_map(map_name)
  save_data.current_level = map_name
  love.filesystem.write("data.lua", table.show(save_data, "save_data"))

  destroy_all()

  game_map = sti("maps/" .. map_name .. ".lua")

  for _, obj in pairs(game_map.layers["Platforms"].objects) do
    spawn_platform(obj.x, obj.y, obj.width, obj.height)
  end

  for _, obj in pairs(game_map.layers["Enemies"].objects) do
    spawn_enemy(obj.x, obj.y)
  end

  local flag = game_map.layers["Flag"].objects[1]
  flag_x = flag.x
  flag_y = flag.y

  local player_start = game_map.layers["Start"].objects[1]
  player_start_x = player_start.x
  player_start_y = player_start.y
  player:setPosition(player_start_x, player_start_y)
end
